﻿//Server
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpServer
{
    public static void Main()
    {
        string input;
        byte[] data = new byte[1024];
        byte[] data1 = new byte[1024];
        //membuka IP dan mengatur Port
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8001);
        //mendefinisikan jenis protokol adalah UDP
        UdpClient newsock = new UdpClient(ipep);
        //input username
        Console.Write("Masukkan username anda : ");
        string username = Console.ReadLine();
        Console.WriteLine("Menunggu klien...");
        //menerima 
        IPEndPoint from = new IPEndPoint(IPAddress.Any, 0);
        data = newsock.Receive(ref from);
        string clientUsername = Encoding.ASCII.GetString(data, 0, data.Length);
        //menuliskan IP client
        Console.WriteLine("Menerima dari : {0}", from.ToString());
        //menuliskan username client
        Console.WriteLine("Username Client : " + Encoding.ASCII.GetString(data, 0, data.Length));
        //mengirim data
        data = Encoding.ASCII.GetBytes(username);
        newsock.Send(data, data.Length, from);

        while (true)
        {
            //menerima
            data = newsock.Receive(ref from);
            Console.WriteLine(clientUsername + " : " + Encoding.ASCII.GetString(data, 0, data.Length));
            Console.Write(username + " : ");
            //mengirim
            input = Console.ReadLine();
            data1 = Encoding.ASCII.GetBytes(input);
            newsock.Send(data1, data1.Length, from);
        }
    }
}